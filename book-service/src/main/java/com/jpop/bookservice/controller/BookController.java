package com.jpop.bookservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpop.bookservice.entities.Book;
import com.jpop.bookservice.service.BookService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 
 * The Class BookController.
 * 
 * @author Ashutosh_Sharma
 */
@RestController
@RequestMapping("/books")
public class BookController {

	/** The book service. */
	@Autowired
	BookService bookService;

	/**
	 * Gets the books.
	 *
	 * @return the books
	 */
	@ApiOperation(value = "View the list of books.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrived books list.") })
	@GetMapping("")
	public List<Book> getBooks() {
		return bookService.getAllBooks();
	}

	/**
	 * Gets the book.
	 *
	 * @param bookId the book id
	 * @return the book
	 */
	@ApiOperation(value = "Search a book with a bookId.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrived book with ID successfull.") })
	@GetMapping("/{book_id}")
	public Book getbook(@PathVariable("book_id") int bookId) {
		return bookService.getBook(bookId);
	}

	/**
	 * Adds the book.
	 *
	 * @param newBook the new book
	 * @return the book
	 */
	@ApiOperation(value = "Add a book.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Book successfully added.") })
	@PostMapping("/")
	public Book addBook(@RequestBody Book newBook) {
		return bookService.addBook(newBook);
	}

	/**
	 * Delete book.
	 *
	 * @return the book
	 */
	@ApiOperation(value = "Delete a book with a bookId.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Book successfully deleted with provided ID.") })
	@DeleteMapping("/{book_id}")
	public int deletebook(@PathVariable("book_id") int bookId) {
		return bookService.deleteBook(bookId);
	}

	/**
	 * Update book.
	 *z
	 * @return the book
	 */
	@ApiOperation(value = "Update a book with a bookId.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Book successfully updated with provided ID.") })
	@PutMapping("/{book_id}")
	public Book updateBook(@PathVariable("book_id") int bookId, @RequestBody Book updatedBook) {
		return bookService.updateBook(bookId, updatedBook);
	}
}
