package com.jpop.bookservice.service;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpop.bookservice.constants.Constants;
import com.jpop.bookservice.entities.Book;
import com.jpop.bookservice.repository.BookRepository;
import com.library.commons.book.BookNotFound;

/**
 * The Class BookService.
 * 
 * @author Ashutosh_Sharma
 */
@Service
public class BookService {

	/** The Constant logger. */
	private Logger logger = LogManager.getLogger(BookService.class);

	/** The book repository. */
	@Autowired
	BookRepository bookRepository;

	/**
	 * Gets the all books.
	 *
	 * @return the all books
	 */
	public List<Book> getAllBooks() {
		return (List<Book>) bookRepository.findAll();
	}

	/**
	 * Gets the book.
	 *
	 * @param bookId the book id
	 * @return the book
	 */
	public Book getBook(int bookId) {
		Optional<Book> book = bookRepository.findById(bookId);
		if (book.isPresent())
			return book.get();
		else {
			logger.info(Constants.BOOK_NOT_EXISTS);
			throw new BookNotFound(Constants.BOOK_NOT_EXISTS);
		}
	}

	/**
	 * Adds the book.
	 *
	 * @param book the book
	 * @return the book
	 */
	public Book addBook(Book book) {
		return bookRepository.save(book);
	}

	/**
	 * Delete book.
	 *
	 * @param bookId the book id
	 * @return deletedBookId
	 */
	public int deleteBook(int bookId) {
		int deletedBookId;
		try {
			this.getBook(bookId);
			bookRepository.deleteById(bookId);
			deletedBookId = bookId;
		} catch (BookNotFound e) {
			logger.info(Constants.BOOK_NOT_EXISTS);
			throw new BookNotFound(Constants.BOOK_NOT_EXISTS);
		}
		return deletedBookId;
	}

	/**
	 * Update book.
	 *
	 * @param bookId      the book id
	 * @param updatedBook the updated book
	 * @return the book
	 */
	public Book updateBook(int bookId, Book updatedBook) {
		try {
			this.getBook(bookId);
			updatedBook.setId(bookId);
			return bookRepository.save(updatedBook);
		} catch (BookNotFound e) {
			logger.info(Constants.BOOK_NOT_EXISTS);
			return null;
		}
	}

}
