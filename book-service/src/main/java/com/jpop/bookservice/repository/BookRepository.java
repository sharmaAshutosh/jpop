package com.jpop.bookservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.jpop.bookservice.entities.Book;

/**
 * The Interface BookRepository.
 * 
 * @author Ashutosh_Sharma
 */
public interface BookRepository extends CrudRepository<Book, Integer> {

}
