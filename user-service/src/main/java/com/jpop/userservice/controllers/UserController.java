package com.jpop.userservice.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpop.userservice.entities.User;
import com.jpop.userservice.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The Class UserController.
 *
 * @author Ashutosh_Sharma
 */
@RestController
@RequestMapping("/users")
public class UserController {

	/** The user service. */
	@Autowired
	UserService userService;

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	@ApiOperation(value = "View the list of Users.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrived Users list.") })
	@GetMapping("")
	public List<User> getUsers() {
		return userService.getAllUsers();
	}

	/**
	 * Gets the user.
	 *
	 * @param userId the user id
	 * @return the user
	 */
	@ApiOperation(value = "Search a User with a userId.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrived user with ID successfull.") })
	@GetMapping("/{user_id}")
	public User getUser(@PathVariable("user_id") int userId) {
		return userService.getUser(userId);
	}

	/**
	 * Adds the user.
	 *
	 * @param newUser the new user
	 * @return the created user
	 */
	@ApiOperation(value = "Register a User.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User successfully Registered.") })
	@PostMapping("/")
	public User addUser(@RequestBody User newUser) {
		return userService.addUser(newUser);
	}

	/**
	 * Delete user.
	 *
	 * @param userId the user id
	 * @return the userId of the deleted user
	 */
	@ApiOperation(value = "Delete a User with a UserId.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User successfully deleted with provided ID.") })
	@DeleteMapping("/{user_id}")
	public int deleteUser(@PathVariable("user_id") int userId) {
		return userService.deleteUser(userId);
	}

	/**
	 * Update user.
	 *
	 * @param userId      the user id
	 * @param updatedUser the updated user
	 * @return the updated user
	 */
	@ApiOperation(value = "Update a User with a UserId.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User successfully updated with provided ID.") })
	@PutMapping("/{user_id}")
	public User updateUser(@PathVariable("user_id") int userId, @RequestBody User updatedUser) {
		return userService.updateUser(userId, updatedUser);
	}
}
