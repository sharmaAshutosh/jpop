package com.jpop.userservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.jpop.userservice.entities.User;

/**
 * The Interface UserRepository.
 * 
 * @author Ashutosh_Sharma
 */
public interface UserRepository extends CrudRepository<User, Integer> {

}
