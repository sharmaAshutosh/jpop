package com.jpop.userservice.service;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpop.userservice.constants.Constants;
import com.jpop.userservice.entities.User;
import com.jpop.userservice.repository.UserRepository;
import com.library.commons.user.UserNotFound;

/**
 * The Class UserService.
 * 
 * @author Ashutosh_Sharma
 */
@Service
public class UserService {

	/** The Constant logger. */
	private Logger logger = LogManager.getLogger(UserService.class);

	/** The User repository. */
	@Autowired
	UserRepository userRepository;

	/**
	 * Gets the all Users.
	 *
	 * @return the all Users
	 */
	public List<User> getAllUsers() {
		return (List<User>) userRepository.findAll();
	}

	/**
	 * Gets the User.
	 *
	 * @param userId the User id
	 * @return the User
	 */
	public User getUser(int userId) {
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent())
			return user.get();
		else {
			logger.info(Constants.USER_NOT_EXISTS);
			throw new UserNotFound(Constants.USER_NOT_EXISTS);
		}
	}

	/**
	 * Adds the User.
	 *
	 * @param User the User
	 * @return the User
	 */
	public User addUser(User user) {
		return userRepository.save(user);
	}

	/**
	 * Delete User.
	 *
	 * @param userId the User id
	 * @return deletedUserId
	 */
	public int deleteUser(int userId) {
		int deletedUserId;
		try {
			this.getUser(userId);
			userRepository.deleteById(userId);
			deletedUserId = userId;
		} catch (UserNotFound e) {
			logger.info(Constants.USER_NOT_EXISTS);
			throw new UserNotFound(Constants.USER_NOT_EXISTS);
		}
		return deletedUserId;
	}

	/**
	 * Update User.
	 *
	 * @param userId      the User id
	 * @param updatedUser the updated User
	 * @return the User
	 */
	public User updateUser(int userId, User updatedUser) {
		try {
			this.getUser(userId);
			updatedUser.setId(userId);
			return userRepository.save(updatedUser);
		} catch (UserNotFound e) {
			logger.info(Constants.USER_NOT_EXISTS);
			return null;
		}
	}

}
