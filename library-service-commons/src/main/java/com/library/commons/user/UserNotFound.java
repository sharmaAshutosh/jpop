package com.library.commons.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The Class UserNotFound.
 * 
 * @author Ashutosh_Sharma
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason="User with provided ID not found.")
public class UserNotFound extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The message. */
	public final String message;

	/**
	 * Instantiates a new User not found.
	 *
	 * @param message the message
	 */
	public UserNotFound(String message) {
		super();
		this.message = message;
	}

}
