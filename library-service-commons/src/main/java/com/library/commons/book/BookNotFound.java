package com.library.commons.book;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The Class BookNotFound.
 * 
 * @author Ashutosh_Sharma
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason="Book with provided ID not found.")
public class BookNotFound extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The message. */
	public final String message;

	/**
	 * Instantiates a new book not found.
	 *
	 * @param message the message
	 */
	public BookNotFound(String message) {
		super();
		this.message = message;
	}

}
