package com.jpop.libraryservice.entites;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.springframework.context.annotation.ComponentScan;

/**
 * Entity to keep record of books issued to a user.
 * 
 * @author Ashutosh_Sharma
 */
@ComponentScan("com.library.commons.book")
@ComponentScan("com.library.commons.user")
@Entity
public class IssueBook {

	/** The id. */
	@Id
	private int id;

	/** The book. */
	@OneToOne(targetEntity = Book.class)
	@JoinColumn(name = "book_id")
	private Book book;

	/** The user. */
	@OneToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id")
	private User user;

	/**
	 * Gets the book.
	 *
	 * @return the book
	 */
	public Book getBook() {
		return book;
	}

	/**
	 * Sets the book.
	 *
	 * @param book the new book
	 */
	public void setBook(Book book) {
		this.book = book;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
