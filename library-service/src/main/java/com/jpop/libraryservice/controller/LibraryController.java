package com.jpop.libraryservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpop.libraryservice.dto.LoginUserDTO;
import com.jpop.libraryservice.entites.Book;
import com.jpop.libraryservice.entites.IssueBook;
import com.jpop.libraryservice.entites.User;
import com.jpop.libraryservice.service.BookService;
import com.jpop.libraryservice.service.IssueBookService;
import com.jpop.libraryservice.service.LoginService;
import com.jpop.libraryservice.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * The Class LibraryController.
 * 
 * @author Ashutosh_Sharma
 */
@RestController
@RequestMapping("/lib")
public class LibraryController {

	/** The login service. */
	@Autowired
	LoginService loginService;

	/** The book service. */
	@Autowired
	BookService bookService;

	/** The user service. */
	@Autowired
	UserService userService;

	@Autowired
	IssueBookService issueBookService;

	/**
	 * Login.
	 *
	 * @param loginUserDTO the login user DTO
	 * @return true, if successful
	 */
	@ApiOperation(value = "Login service using userId and password.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User successfully logged-in.") })
	@GetMapping("")
	public ResponseEntity<String> login(@RequestBody LoginUserDTO loginUserDTO) {
		return loginService.authenticateUser(loginUserDTO);
	}

	/**
	 * Gets the all books.
	 *
	 * @return the all books
	 */
	@ApiOperation(value = "Fetch all books from database.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Books succcessfully fetched.") })
	@GetMapping("/books")
	public ResponseEntity<List<Book>> getAllBooks() {
		return ResponseEntity.ok(bookService.getAllBooks());
	}

	/**
	 * Gets the book.
	 *
	 * @param bookId the book id
	 * @return the book
	 */
	@ApiOperation(value = "Fetch a book with given Id from database.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Book succcessfully fetched.") })
	@GetMapping("/books/{book_id}")
	public ResponseEntity<Book> getBook(@PathVariable("book_id") int bookId) {
		return bookService.getBook(bookId);
	}

	/**
	 * Add the book.
	 *
	 * @param book the book
	 * @return the book
	 */
	@ApiOperation(value = "Add a book")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Book succcessfully added.") })
	@PostMapping("/users")
	public ResponseEntity<Book> addBook(@RequestBody Book book) {
		return bookService.addBook(book);
	}

	/**
	 * Delete book.
	 *
	 * @param bookId the book id
	 * @return the response entity
	 */
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Book succcessfully Deleted.") })
	@DeleteMapping("/books/{book_id}")
	public ResponseEntity deleteBook(@PathVariable("book_id") int bookId) {
		return issueBookService.deleteABook(bookId);
	}

	/**
	 * Gets the all users.
	 *
	 * @return the all users
	 */
	@ApiOperation(value = "Fetch all users.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Users succcessfully fetched.") })
	@GetMapping("/users")
	public ResponseEntity<List<User>> getAllUsers() {
		return ResponseEntity.ok(userService.getAllUsers());
	}

	/**
	 * Gets the user.
	 *
	 * @param userId the user id
	 * @return the user
	 */
//	@ApiOperation(value = "Fetch user details with userId.")
//	@ApiResponses(value = { @ApiResponse(code = 200, message = "User details succcessfully fetched.") })
//	@GetMapping("/users/{user_id}")
//	public ResponseEntity<User> getUser(@PathVariable int userId) {
//		return userService.getuser(userId);
//	}

	/**
	 * Update user.
	 *
	 * @param user the user
	 * @return the response entity
	 */
	@ApiOperation(value = "Update user details with userId.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User details succcessfully updated.") })
	@PostMapping("/users/{user_id}")
	public ResponseEntity<User> updateUser(@RequestBody User user) {
		return userService.updateUser(user);
	}

	/**
	 * Delete user.
	 *
	 * @param user the user
	 * @return the response entity
	 */
	@ApiOperation(value = "Release all books for that user_id in library table and delete user.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "User succcessfully deleted.") })
	@DeleteMapping("/users/{user_id}")
	public ResponseEntity<User> deleteUser(@PathVariable("user_id") int userId) {
		return issueBookService.releaseAllBooksWithUser(userId);
	}

	/**
	 * Issue book to user.
	 *
	 * @param userId the user id
	 * @param bookId the book id
	 * @return the response entity
	 */
	@ApiOperation(value = "Issue a book with bookId for a user with a userId.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Book is sucessfully issued to User.") })
	@GetMapping("/users/{user_id}/books/{book_id}")
	public ResponseEntity issueBookToUser(@PathVariable("user_id") int userId, @PathVariable("book_id") int bookId) {
		return issueBookService.issueBookToUser(userId, bookId);
	}

	/**
	 * Gets the issued books for user.
	 *
	 * @param userId the user id
	 * @return the issued books for user
	 */
	@ApiOperation(value = "View user profile with all issued books.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Books issued to user successfully fetched.") })
	@GetMapping("/users/{user_id}")
	public ResponseEntity<List<IssueBook>> getIssuedBooksForUser(@PathVariable("user_id") int userId) {
		return ResponseEntity.ok(issueBookService.getBooksIssuedWithUser(userId));
	}

}
