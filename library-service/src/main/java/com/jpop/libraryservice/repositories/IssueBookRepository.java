package com.jpop.libraryservice.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jpop.libraryservice.entites.Book;
import com.jpop.libraryservice.entites.IssueBook;
import com.jpop.libraryservice.entites.User;

/**
 * 
 * IssueBook Repository.
 * 
 * @author Ashutosh_Sharma
 */
public interface IssueBookRepository extends CrudRepository<IssueBook, Integer> {

	/**
	 * Find by user.
	 *
	 * @param user the user
	 * @return the list
	 */
	public List<IssueBook> findByUser(User user);

	/**
	 * Delete by user.
	 *
	 * @param user the user
	 */
	public void deleteByUser(User user);

	/**
	 * Delete by book.
	 *
	 * @param book the book
	 */
	public void deleteByBook(Book book);
}
