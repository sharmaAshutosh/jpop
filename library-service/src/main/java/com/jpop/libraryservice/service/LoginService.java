/**
 * 
 */
package com.jpop.libraryservice.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.jpop.libraryservice.dto.LoginUserDTO;
import com.jpop.libraryservice.entites.User;

/**
 * The Class LoginUtil.
 *
 * @author Ashutosh_Sharma
 */
@Service
public class LoginService {

	/** The Constant logger. */
	private Logger logger = LogManager.getLogger(LoginService.class);

	/** The rest template. */
	@Autowired
	RestTemplate restTemplate;

	/**
	 * Authenticate user.
	 *
	 * @param loginUserDTO the login user DTO
	 * @return the response entity
	 */
	public ResponseEntity<String> authenticateUser(LoginUserDTO loginUserDTO) {

		int userId = loginUserDTO.getUserId();
		User user = null;

		final String loginUri = "http://localhost:1002/users/" + Integer.toString(userId);

		HttpHeaders headers = new HttpHeaders();
		String message = null;

		try {
			user = restTemplate.getForObject(loginUri, User.class);
		} catch (RestClientException e) {
			logger.info("Login Service - User not Found!");
		}

		if (user != null && user.getPassword().equals(loginUserDTO.getPassword())) {
			headers.add("SUCCESS", "true");
			message = "Login Successful!";
		} else {
			headers.add("SUCCESS", "false");
			message = "Login failed!";
		}

		return new ResponseEntity<>(message, headers, HttpStatus.OK);
	}
}
