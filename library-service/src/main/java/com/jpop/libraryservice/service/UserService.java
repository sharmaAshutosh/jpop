package com.jpop.libraryservice.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.jpop.libraryservice.constants.Constants;
import com.jpop.libraryservice.entites.User;
import com.library.commons.user.UserNotFound;

/**
 * The Class UserService.
 */
@Service
public class UserService {

	/** The Constant logger. */
	private Logger logger = LogManager.getLogger(UserService.class);

	private static final String USER_API_URL = "http://localhost:1002/users/";

	/** The rest template. */
	@Autowired
	RestTemplate restTemplate;

	/**
	 * Gets the user.
	 *
	 * @param userId the user id
	 * @return the user
	 */
	public ResponseEntity<User> getuser(int userId) {
		User user = null;

		final String url = USER_API_URL + userId;

		try {
			user = restTemplate.getForObject(url, User.class);
		} catch (UserNotFound u) {
			logger.info(u.getMessage());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (RestClientException e) {
			logger.info(Constants.USER_SERVICE_NOT_WORKING);
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
		return ResponseEntity.ok(user);
	}

	/**
	 * Gets the all users.
	 *
	 * @return the all users
	 */
	public List<User> getAllUsers() {
		List<User> users = null;

		try {
			users = restTemplate.getForObject(USER_API_URL, List.class);
		} catch (RestClientException e) {
			logger.info(Constants.USER_SERVICE_NOT_WORKING);
		}
		return users;
	}

	public ResponseEntity deleteuser(int userId) {

		final String url = USER_API_URL + userId;

		try {
			restTemplate.delete(url);
		} catch (UserNotFound u) {
			logger.info(u.getMessage());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (RestClientException e) {
			logger.info(Constants.USER_SERVICE_NOT_WORKING);
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
		return ResponseEntity.ok().build();
	}

	public ResponseEntity updateUser(User user) {

		HttpEntity<User> request = new HttpEntity<>(user);

		final String url = USER_API_URL + user.getId();

		try {
			restTemplate.postForObject(url, request, User.class);
		} catch (RestClientException e) {
			logger.info(Constants.USER_SERVICE_NOT_WORKING);
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
		return ResponseEntity.ok().build();
	}

}
