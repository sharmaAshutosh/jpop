package com.jpop.libraryservice.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.jpop.libraryservice.constants.Constants;
import com.jpop.libraryservice.entites.Book;
import com.library.commons.book.BookNotFound;

/**
 * BookService.
 * 
 * @author Ashutosh_Sharma
 */
@Service
public class BookService {

	/** The Constant logger. */
	private Logger logger = LogManager.getLogger(BookService.class);

	/** The rest template. */
	@Autowired
	RestTemplate restTemplate;

	/** The Constant BOOK_API_URL. */
	private static final String BOOK_API_URL = "http://localhost:1001/books/";

	/**
	 * Gets the all books.
	 *
	 * @return the all books
	 */
	public List<Book> getAllBooks() {
		List<Book> books = null;

		try {
			books = restTemplate.getForObject(BOOK_API_URL, List.class);
		} catch (RestClientException e) {
			logger.info(Constants.BOOK_SERVICE_NOT_WORKING);
		}
		return books;
	}

	/**
	 * Gets the book.
	 *
	 * @param bookId the book id
	 * @return the book
	 */
	public ResponseEntity<Book> getBook(int bookId) {
		Book book = null;

		final String url = BOOK_API_URL + bookId;

		try {
			book = restTemplate.getForObject(url, Book.class);
		} catch (BookNotFound b) {
			logger.info(b.getMessage());
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		} catch (RestClientException e) {
			logger.info(Constants.BOOK_SERVICE_NOT_WORKING);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		} catch (Exception e) {
			ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
		return ResponseEntity.ok(book);
	}

	/**
	 * Adds the book.
	 *
	 * @param book the book
	 * @return the response entity
	 */
	public ResponseEntity<Book> addBook(Book book) {
		Book createdBook = new Book();

		try {
			createdBook = restTemplate.postForObject(BOOK_API_URL, book, Book.class);
		} catch (RestClientException e) {
			logger.info(Constants.BOOK_SERVICE_NOT_WORKING);
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
		return ResponseEntity.ok(createdBook);
	}

	/**
	 * Delete book.
	 *
	 * @param bookId the book id
	 * @return the book
	 */
	public ResponseEntity deleteBook(int bookId) {

		final String url = BOOK_API_URL + bookId;

		try {
			restTemplate.delete(url);
		} catch (BookNotFound b) {
			logger.info(b.getMessage());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (RestClientException e) {
			logger.info(Constants.BOOK_SERVICE_NOT_WORKING);
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
		}
		return ResponseEntity.ok().build();
	}

}
