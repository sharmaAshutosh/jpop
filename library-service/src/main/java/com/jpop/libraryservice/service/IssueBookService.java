package com.jpop.libraryservice.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jpop.libraryservice.entites.Book;
import com.jpop.libraryservice.entites.IssueBook;
import com.jpop.libraryservice.entites.User;
import com.jpop.libraryservice.repositories.IssueBookRepository;

/**
 * The Class IssueBookService.
 */
@Service
public class IssueBookService {

	/** The logger. */
	private Logger logger = LogManager.getLogger(BookService.class);

	/** The book service. */
	@Autowired
	BookService bookService;

	/** The user service. */
	@Autowired
	UserService userService;

	/** The issue book repository. */
	@Autowired
	IssueBookRepository issueBookRepository;

	/**
	 * Issue book to user.
	 *
	 * @param bookId the book id
	 * @param userId the user id
	 */
	public ResponseEntity issueBookToUser(int bookId, int userId) {
		IssueBook ib = new IssueBook();
		ResponseEntity<Book> bookResponse = bookService.getBook(bookId);

		if (bookResponse.getStatusCode() == HttpStatus.OK) {
			ib.setBook(bookResponse.getBody());
		}

		ResponseEntity<User> userResponse = userService.getuser(userId);

		if (userResponse.getStatusCode() == HttpStatus.OK) {
			ib.setUser(userResponse.getBody());
		}
		issueBookRepository.save(ib);
		String responseMessage = "Book with id" + bookId + "issued to user with Id" + userId;
		logger.info(responseMessage);
		return ResponseEntity.ok(responseMessage);
	}

	/**
	 * Gets the books issued with user.
	 *
	 * @param userId the user id
	 * @return the books issued with user
	 */
	public List<IssueBook> getBooksIssuedWithUser(int userId) {

		ResponseEntity<User> userResponse = userService.getuser(userId);
		User user = null;

		if (userResponse.getStatusCode() == HttpStatus.OK) {
			user = userResponse.getBody();
		}

		logger.info("Got books issued to a user with userId" + userId);
		return issueBookRepository.findByUser(user);
	}

	/**
	 * Release all books with user.
	 *
	 * @param userId the user id
	 * @return
	 */
	public ResponseEntity<User> releaseAllBooksWithUser(int userId) {
		ResponseEntity<User> userResponse = userService.getuser(userId);
		User user = null;

		if (userResponse.getStatusCode() == HttpStatus.OK) {
			user = userResponse.getBody();
		}

		issueBookRepository.deleteByUser(user);
		logger.info("Relesed books issued to a user with userId" + userId);

		logger.info("User deleted with UserId: " + userId);
		return userService.deleteuser(userId);
	}

	/**
	 * Delete A book.
	 *
	 * @param bookId the book id
	 * @return the response entity
	 */
	public ResponseEntity deleteABook(int bookId) {
		// delete book record from library
		ResponseEntity<Book> bookResponse = bookService.getBook(bookId);
		Book book = null;
		if (bookResponse.getStatusCode() == HttpStatus.OK) {
			book = bookResponse.getBody();
		} else {
			logger.error("Book with specified id not found");
			return ResponseEntity.badRequest().build();
		}

		issueBookRepository.deleteByBook(book);

		// delete book from book table
		bookService.deleteBook(bookId);
		return ResponseEntity.ok().build();

	}

}
