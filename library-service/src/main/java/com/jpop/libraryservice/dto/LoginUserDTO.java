package com.jpop.libraryservice.dto;

import org.springframework.stereotype.Component;

/**
 * The Class LoginDTO.
 *
 * @author Ashutosh_Sharma
 */
@Component
public class LoginUserDTO {
	
	/** The user id. */
	private int userId;
	
	/** The password. */
	private String password;

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
}
