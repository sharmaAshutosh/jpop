package com.jpop.libraryservice.constants;

public class Constants {
	public final static String BOOK_SERVICE_NOT_WORKING = "Error on book service API.";
	public final static String USER_SERVICE_NOT_WORKING = "Error on user service API.";
}
